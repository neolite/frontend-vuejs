# Welcome to BrainQ Frontend/Vue.js Developer Test

## Setup
### Environment
This is a Laravel/Vue.js project. To run it you need either one:
1. Local PHP/Laravel environment
2. Docker environment (recommended [Laradock](http://laradock.io/))

### Dependencies
Install composer dependencies `composer install`.

Install node dependencies `npm install`.


## Usage
To run the project you can use the built in PHP server using:
`php artisan serve`
or if you used Laradock, simply open [http://localhost](http://localhost) in the browser.

Every time you change a js/css file you need to run `npm run dev`. This will compile the Vue.js components to app.js file located at _public/js/app.js_.

## Task
1. [Fork this repository](https://bitbucket.org/brainq/frontend-vuejs/fork) to your BitBucket/Github account.
2. Edit _resources/assets/js/components/Example.vue_ file to look like the [provided design](https://app.zeplin.io/project/5a48cc2d77bdbc7719aa986f/screen/5a48cceccfac9fd03afde32b). You should have received an invitation link to Zeplin.
3. Commit your changes
4. Send an email back with a link to the repository.

### Remarks
The project uses Bootstrap 3.x. Please use bootstrap classes and components.
Styling should be written in _style_ tag in Example.vue file.